/********************************************************************************************
 * 	Factorial Lib v0.01					Developed by HasH
 *	----------------------------------------------------------------------------	
 *	Functions related to factorial of integers
 *
 *	fact(num)		: return num! [0<=num<=20]
 *	factDiv(num, div)	: return no.of div factors in num!	
 *	trailZero(num)		: return no.of trailing zeroes in num!
 *	factlen(num)		: return length of num!
 *
 *	To be added:
 *	----------------------------------------------------------------------------
 *
 *
 * ******************************************************************************************/

#include<stdio.h>
#include<stdint.h>
#include<math.h>

// Returns the factorial of number 'num' using a normal iterative method (0<=num<=20)
// Bigger factorials are too large to fit into 64 bit unsigned integers
uint64_t fact(const unsigned int num){
	int i = 0; uint64_t num_fact = 1; 	//initializing factorial as 1
	for(i=1;i<=num;i++)
		num_fact *= i;			//fact(n) = n*fact(n-1)

	return num_fact;			
}

//Returns the no.of divs that are factors of num!, => num!/(div)^x is an integer
unsigned int factDiv(const unsigned int num, const unsigned int div){
	int xdiv = num;				//xdiv = num since num is defined as constant
	int nDiv = 0;
	
	while(xdiv>div){			//while there are more div in xdiv
		xdiv /= div;			
		nDiv += xdiv;			//summing up no.of divisors
	}

	return nDiv;
}

//Returns the no.of trailing zeroes in num!
unsigned int trailZero(const unsigned int num){
	return factDiv(num, 5);			//Trailing zeroes are equal to the number of 5 factors in num!
}


//Returns the length of num! with high precision
unsigned int factlen(const long double num){
	if(x==0){ return 1; }			//0!=1 and that is length 1

	long double pi = 3.14159265358979323846264338327L;			//For high precision
	long double val = (x*log(x)- x + 0.5*log(2*pi*x) + 1/(12*x))/log(10);	//Stirlings approximation
	return ((uint64_t) val+1);		//Return the length of num!
}
