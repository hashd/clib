#include<stdio.h>
#include "../lib/bitlib.h"
#define myVal 127
#define intSize 32
#define largeVal 1000000000000000000
#define longSize 64

int main(){
	int valBit[intSize], largeBit[longSize];
	bSetArray(myVal, intSize, valBit);
	bSetArray(largeVal, longSize, largeBit);
	return 0;
}
