#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<stdint.h>

uint64_t M[2][2] = {{1,0},{0,1}};

uint64_t rFib(const unsigned int n){
	if(n==0||n==1)
		return 1;

	return rFib(n-1) + rFib(n-2);
}

uint64_t Fib(const unsigned int n){
	uint64_t prev = 1, curr = 1, val = 1; 

	int i = 0;
	for(i = 1; i<n; i++){
		val = curr + prev; prev = curr; curr = val;
	}

	return val;
}

long double fibRatio(const unsigned int n){
	uint64_t prev = 1, curr = 1, val = 1;
	
	int i = 0;
	for(i = 1; i<n; i++){
		val = curr + prev; prev = curr; curr = val;
	}

	return ((long double) val*1.0)/prev;
}

void matmul(uint64_t M[][2], uint64_t N[][2]){
	uint64_t a = M[0][0], b = M[0][1], c = M[1][0], d = M[1][1];
	M[0][0] = a*N[0][0] + b*N[0][1]; M[1][0] = a*N[1][0] + b*N[1][1];
	M[0][1] = c*N[0][0] + d*N[0][1]; M[1][1] = c*N[1][0] + d*N[1][1];
	return;
}

void matPow(int n){
	if(n>1){
		matPow(n/2);
		matmul(M,M);
	}
	
	if(n&1){
		uint64_t ind[2][2] = {{1,1}, {0,1}};
		matmul(M,ind);
	}
}

void reset(){
	M[0][0] = 1; M[0][1] = 0; M[1][0] = 0; M[1][1] = 1;
}

uint64_t mfib(int n){
	matPow(n-1);
	return M[0][0];
}

int main(){
	printf("%llu\n", mfib(6));
	reset();
	return 0;
}

