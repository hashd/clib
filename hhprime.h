/*-----------------------------------------------------------------------------------------
 *
 *	Version 0.1 Developed by HasH
 *	THE PRIME NUMBER LIBRARY
 *		All functions related to the concept of prime numbers to be implemented here.
 *
 *
 *
 *
 *
 *
 *
 * --------------------------------------------------------------------------------------*/
#include<stdlib.h>
#include<math.h>
#include<stdbool.h>

void printArray(int *arr, int arrSize){
	int i = 0;
	for(i=0;i<arrSize;i++)
		if(arr[i])
			printf("%d, ", i);
	printf("\n");
	return;
}

/*---------------------------------------GCD--------------------------------------------*/
int euclideanGCD(int a, int b){
	int tmp;
	//Non-recursive method using iteration ... hopefully a pinch faster
	while(b!=0){
		tmp = b; b = a%b; a = tmp;
	}

	return a;
}

int extEuclid(int a, int b, int* x, int* y){
	//recursive method in the extended format => ax + by = 1 (a,b values are also computed)
	//should be useful in computing RSA keys if you ever come across implementing that
	if(b==0){
		*x = 1; *y = 0; return a;
	}

	int gcd = extEuclid(b, a%b, x, y);
	int tmp = *y; *y = *x - (a/b)*tmp; *x = tmp;

	return gcd;
}
/*---------------------------------------------------------------------------------------*/

/*-------------------------------------Modular Exponentiation----------------------------*/
uint64_t modExp(uint64_t base, uint64_t exp, uint64_t mod){
	uint64_t result = 1;
	while(exp > 0){
		if(exp&1)
			result = (result*base)%mod;
		
		exp = exp>>1;
	       	base = (base*base)%mod;
	}

	return result;
}
/*---------------------------------------------------------------------------------------*/

/*-------------------------------------PRIME PROBABILITY CHECK---------------------------*/
bool fermatTest(int N){
	if(modExp(2,N-1,N)==1 && modExp(3,N-1,N)==1 && modExp(5,N-1,N) == 1)
		return true;
	else
		return false;
}

bool rmTest(uint64_t x, int k){
	if(x%2==0||x%3==0||x==1||x==0)
		return 0;

	uint64_t xmin = x-1, count = 0, i, j;
	while(xmin%2==0){
		xmin = xmin>>1;
		count++;
	}

	srand(time(NULL));
	for(i=0;i<k;i++){
		uint64_t rpime = (rand()%(x-4)) + 2;
		uint64_t mod   = modExp(rpime, xmin, x);
		
		int state = 0;
		//printf("%llu\n",mod);
		if(mod==1||mod==(x-1)){
		      	continue; 
		}
		
		for(j=0;j<count;j++){
			mod = (mod * mod)%x;
			//printf("%llu\n",mod);
			if(mod==1) { return 0; }
			if(mod==x-1) { state = 1; break; }
		}

		if(state == 0)
			return 0;
	}
	return 1;
}
/*---------------------------------------------------------------------------------------*/

/*-------------------------------------PRIME SIEVING-------------------------------------*/
void sieveOfEratosthenes(int *arr, int arrSize){
	int i,j;
	for(i=1;i<arrSize;i++)
		arr[i] = 1;

	arr[1] = 0; arr[0] = 0;

	int sqrtval = sqrt(arrSize);
	for(i=2;i<=sqrtval;i++)
		if(arr[i])
			for(j=arrSize/i;j>=i;j--)
				if(arr[j])
					arr[i*j] = 0;

	return;
}

int bPrimeSieve(int* primelist, const unsigned int max){ //max>=4 please
	int i, j; bool* primes = (bool *) calloc(max+1, sizeof(bool));
	
	for(i=0;i<=max;i++)
		primes[i] = true;
	primes[0] = false; primes[1] = false;

	int sqrtval = sqrt(max);
	for(i=4;i<=max;i++)
		if(i%2==0||i%3==0)
			primes[i]=false;
	
	//int* primelist = (int *) calloc(max, sizeof(int)); 
	int count = 2; 	primelist[0] = 2; primelist[1] = 3;
	for(i=5;i<=sqrtval;i++)	
		if(primes[i])
			for(j=max/i;j>=0;j--)
				if(primes[j]&&(primes[i*j] = false));
	
	for(i=5;i<=max;i++)
		if(primes[i])
			primelist[count++] = i;	

	return count;
}
/*---------------------------------------------------------------------------------------*/

void modSieveOfEratosthenes(int num){
	int arr[] = {};
	return;
}

/*---------------------------------------------------------------------------------------*/
