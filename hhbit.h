/*---------------------------------------------------------------------------------------------------------
 *
 * 	Version 0.00.1 	Developed by HasH
 *	BITWISE TWEAKS LIBRARY
 *	----------------------
 *	
 *	SWAP(x,y) 		: Swap x, y without using a temporary variable
 *	bReverse(x)		: Reverses the bits of a 32 bit unsigned integer
 *	bIs2Power(x)		: Checks if the given integer is a perfect power of 2
 *	bLeadingZeroCount(x)	: Returns the number of leading zeros in the integer
 *	bCountSetBits(x)	: Returns the number of bits set to 1
 *
 *	To Be Added:
 *	Library is under construction and has scope for expansion 
 *
 * ------------------------------------------------------------------------------------------------------*/
#ifndef SWAP
#define SWAP(a,b) ((&(a)==&(b)||(((a)^=(b)),((b)^=(a)),((a)^=(b)))));
#endif

// Prints the bitArray created by the bitlib in a user understandable way to help debug issues at times
void bPrintArray(int *bitX, int maxLen){
	int i = 0;
	for(i=maxLen-1;i>=0;i--)
		printf("%d", bitX[i]);
	printf("\n");
}

// 
void bSetArray(unsigned long long x, int maxLen, int *bitX){
	int i = 0;
	for(i=0; i<maxLen; i++){
		bitX[i] = x&1;
		x>>=1;
	}
	bPrintArray(bitX, maxLen);
}

unsigned int bReverse(unsigned int x){
	x = (((x & 0xaaaaaaaa) >> 1)|((x & 0x55555555) << 1));
	x = (((x & 0xcccccccc) >> 2)|((x & 0x33333333) << 2));
	x = (((x & 0xf0f0f0f0) >> 4)|((x & 0x0f0f0f0f) << 4));
	x = (((x & 0xff00ff00) >> 8)|((x & 0x00ff00ff) << 8));
	return ((x>>16)|(x<<16));
}

int bIs2Power(int x){
	return x&(x-1);
}

unsigned int bLeadingZeroCount(unsigned int x){
	x |= (x>>1);
	x |= (x>>2);
	x |= (x>>4);
	x |= (x>>8);
	x |= (x>>16);
	return 32-bCountSetBits(x);
}

int bCountSetBits(unsigned int x){
	int c;
	for(c=0;x;c++)
		x &= (x-1);
	return x;
}

int bModulusDivision(unsigned int x){
	//Under Construction
	return 0;
}
