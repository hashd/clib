#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include "multisort.h"
#define RANGE 40000

void arrayInit(int* arr,int arrSize){
	int i;
	for(i=0;i<arrSize;i++)
		arr[i] = rand()%RAND_MAX + 1;
}

int main(){
	int arr[RANGE];
	srand(time(NULL));
	
	time_t init, end;
	init = time(NULL);
	arrayInit(arr, RANGE);
	bubbleSort(arr, RANGE);
	end = time(NULL);
	printf("%lf\n",difftime(end, init));

	init = time(NULL);
	arrayInit(arr, RANGE);
	cocktailSort(arr, RANGE);
	end = time(NULL);
	printf("%lf\n",difftime(end, init));

	init = time(NULL);
	arrayInit(arr, RANGE);
	gnomeSort(arr, RANGE);
	end = time(NULL);
	printf("%lf\n",difftime(end, init));

	init = time(NULL);
	arrayInit(arr, RANGE);
	insertionSort(arr, RANGE);
	end = time(NULL);
	printf("%lf\n",difftime(end, init));

	init = time(NULL);
	arrayInit(arr, RANGE);
	quickSort(arr, RANGE);
	end = time(NULL);
	printf("%lf\n",difftime(end, init));

	init = time(NULL);
	arrayInit(arr, RANGE);
	mergeSort(arr, RANGE);
	end = time(NULL);
	printf("%lf\n",difftime(end, init));


	return 0;
}
