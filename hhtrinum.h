#include<stdio.h>
#include<stdint.h>
#include<stdbool.h>
#include<math.h>

uint64_t nTriNum(const int num){
	return (num*(num+1))/2;
}

bool isTriNum(const int num){
	int x = 8*num + 1;
	int xsqrt = sqrt(x);

	return (x==(xsqrt*xsqrt));
}
