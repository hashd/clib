#ifndef _STDIO_H_
#define _STDIO_H_
#include<stdio.h>
#endif

typedef struct node{
	int data;
	struct node* next;
} Node;

class List{
	private:
		int numOfElements;
		Node *head;
		Node *tail;

	public:
		List(){
			numOfElements = 0;
			head = NULL;
			tail = NULL;
		}

		~List(){
		}

		int InsertNext(Node *, int);
		int InsertFront(int);
		int InsertBack(int);
		int Remove(Node *);
		void Print();
};

int List::InsertNext(Node *element, int data){
	Node *newNode;
	if((newNode = (Node *)malloc(sizeof(Node)))!=NULL)
		return -1;

	newNode->data = data;
	if(element==NULL){
		if(numOfElements==0)
			tail = newNode;
		newNode->next = head;
		head = newNode;
	}
	else{
		if(element->next==NULL)
			tail = newNode;
		newNode->next = element->next;
		element->next = newNode;
	}

	numOfElements++;
	Print();
	return 0;
}

int List::InsertFront(int data){
	Node *newNode;
	if((newNode = (Node *)malloc(sizeof(Node)))!=NULL)
		return -1;

	newNode->data = data;
	newNode->next = head;
	head = newNode;

	if(tail==NULL)
		tail = newNode;
			
	numOfElements++;
	return 0;
}

int List::InsertBack(int data){
	return InsertNext(tail, data);
}

int List::Remove(Node* element){
	if(!numOfElements)
		return 0;

	Node *newNode = (Node *)malloc(sizeof(Node));
	while(newNode->next!=element)
		newNode = newNode->next;

	Node *tmp = newNode->next;
	newNode->next = newNode->next->next;

	if(newNode->next==NULL)
		tail = newNode;
	
	free(tmp);
	numOfElements--;
	return 1;
}

void List::Print(){
	if(numOfElements==0){
		printf("Blank linked list\n");
		return;
	}

	Node *newNode = (Node *)malloc(sizeof(Node));
	printf("List:\n");
	while(newNode!=NULL)
		printf("%d\t",newNode->data);
	return;
}
