/*---------------------------------------------------------------------------------------------------------
 *
 *	Fibonacci Library  Version 0.01						Developed by HasH
 *	------------------------------------------------------------------------------------------
 *	Functions related to the fibonacci series
 *
 *	rFib(n) 		: return nth fibonacci number (recursive implementation)
 *	Fib(n)			: return nth fibonacci number (iterative implementation)
 *	lFib(n)			: return nth fibonacci number (binary log implementation using lkp table)
 *	fibRatio(n)		: return fib(n+1)/fib(n) ~ Golden Ratio 1.61......
 *
 * 	To be Added
 * 	------------------------------------------------------------------------------------------
 *
 *
 * -----------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<stdint.h>

uint64_t rFib(const unsigned int n){
	if(n==0||n==1)
		return 1;

	return rFib(n-1) + rFib(n-2);
}

uint64_t Fib(const unsigned int n){
	uint64_t prev = 1, curr = 1, val = 1; 

	int i = 0;
	for(i = 1; i<n; i++){
		val = curr + prev; prev = curr; curr = val;
	}

	return val;
}

long double fibRatio(const unsigned int n){
	uint64_t prev = 1, curr = 1, val = 1;
	
	int i = 0;
	for(i = 1; i<n; i++){
		val = curr + prev; prev = curr; curr = val;
	}

	return ((long double) val*1.0)/prev;
}

void fibtimes(uint64_t a, uint64_t b, uint64_t* c, uint64_t* d){
	uint64_t x = *c, y = *d;
	*c = a*(x+y) + b*x;
	*d = a*x + b*y;
}

uint64_t lFib(int x){
	uint64_t a = 1, b = 0, c = 0, d = 1;
	while(x>0){
		if(x&1)
			fibtimes(a,b,&c,&d);
		x/=2;
		fibtimes(a,b,&a,&b);
	}	
	return c;
}

