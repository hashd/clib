/****************************************************************************************************
 *
 * 	Version 1.2  Developed by HasH	
 * 	Multiple sorts implemented in C over a integer Array arr, with length arrSize using generic functions like
 *	swap and printArray to remove redundancy of code
 *	
 *	1.  Bubble Sort		- http://en.wikipedia.org/wiki/Bubble_sort
 *	2.  Cocktail Sort	- http://en.wikipedia.org/wiki/Cocktail_sort
 *	3.  Odd Even Sort	- http://en.wikipedia.org/wiki/Odd-even_sort
 *	4.  Comb Sort		- http://en.wikipedia.org/wiki/Comb_sort
 *	5.  Gnome Sort		- http://en.wikipedia.org/wiki/Gnome_sort
 *	6.  Quick Sort		- http://en.wikipedia.org/wiki/Quicksort
 *	7.  Merge Sort		- http://en.wikipedia.org/wiki/Merge_sort
 *	8.  Selection Sort	- http://en.wikipedia.org/wiki/Selection_sort
 *	9.  Bingo Sort		- http://xlinux.nist.gov/dads//HTML/bingosort.html
 *	10. Insertion Sort	- http://en.wikipedia.org/wiki/Insertion_sort
 *	11. Shell Sort		- http://en.wikipedia.org/wiki/Shell_sort
 *
 *	To be added soon:
 *	Cycle Sort, Tournament Sort, Heap Sort and many more
 *
 * 	Usage:
 * 		Call $SortName($Array, $Array_Size) to have an inplace sort 
 * 		
 *	Recent Changes:
 *	- Converted Gnome Sort to optimized gnome sort using the optimization technique in the wiki page
 *	- Merge sort is now slightly inplace and uses intermediate arrays only temporarily 
 *	- v1.1: Added selection sort, bingo sort, insertion sort, shell sort
 *	- v1.2: Separated the functions as a single header file containing the implementations 
 *
 * *****************************************************************************************************/

//The generic swap function for use anywhere
void swap(int *x, int *y){
	*x = *x + *y;
	*y = *x - *y;
	*x = *x - *y;
}

//Prints an integer array provide the address of the first element and the size
void printArray(int *arr, int arrSize){
	int i=0;
	for(i=0; i<arrSize; i++)
		printf("%3d ",arr[i]);
	printf("\n");
}

//Swap using a temporary variable, definition is also a bit different
void pswap(int *v, int i, int j){
	int tmp = v[j];
	v[j] = v[i];
	v[i] = tmp;
}

//--------------------------------BUBBLE SORT-------------------------------------------------
//Bubble Sort --- Now depreciated and not taught as it is not considered anywhere near optimal
void bubbleSort(int *arr, int arrSize){
	int i, j;
	for(i=0;i<arrSize-1;i++)		//For each iteration, the max element is sent to the end
		for(j=0;j<arrSize-1-i;j++)	//The swapper iteration, compares with the immediate element
			if(arr[j]>arr[j+1])
				swap(arr+j,arr+j+1);
	return;
}
//--------------------------------------------------------------------------------------------

//-------------------------------INSERTION SORT-----------------------------------------------
//Insertion Sort
void insertionSort(int *arr, int arrSize){
	int i, j, key;

	for(i=1;i<arrSize;i++){
		key = arr[i];			//The key which needs to be inserted into the sorted part of the arr
		j = i-1;			//Pivot to the last element of the sorted sub array
		while(j>=0 && arr[j]>key){	//Move elements forward until the correct place for the key is found
			arr[j+1] = arr[j];
			j--;
		}
		arr[j+1] = key;			//Place the key in its position and increase the sorted subarray
	}
}
//--------------------------------------------------------------------------------------------

//------------------------------SELECTION SORT------------------------------------------------
//Selection sort
void selectionSort(int *arr, int arrSize){
	int i, j, min, minPos;
	for(i=0;i<arrSize-1;i++){
		min = arr[i];
		minPos = i;
		for(j=i+1;j<arrSize;j++)
			if(arr[j]<min){
				min = arr[j];
				minPos = j;
			}
		pswap(arr, i, minPos);
	}
}
//--------------------------------------------------------------------------------------------

//-------------------------------BINGO SORT---------------------------------------------------
//Bingo Sort
void bingoSort(int *arr, int arrSize){
	int i, j, min, minPos[arrSize], count;
	for(i=0;i<arrSize-1;){
		count = 0;
		min = arr[i];
		minPos[count++] = i;
		for(j=i+1;j<arrSize;j++)
			if(arr[j]<min){
				count = 0;
				min = arr[j];
				minPos[count++] = j;
			}
			else if(arr[j]==min)
				minPos[count++] = j;

		for(j=0;j<count;j++,i++)
			pswap(arr, i, minPos[j]);
	}
}
//--------------------------------------------------------------------------------------------

//------------------------------COCKTAIL SORT-------------------------------------------------
//Cocktail Sort --- the 2-way bubble sort, slightly better but not the optimal solution and not considered better than the rival Insertion Sort
void cocktailSort(int *arr, int arrSize){
	int i = 0, j = 0, swapped = 1;
	while(swapped==1){
		swapped = 0;
		for(j=i;j<arrSize-1-i;j++)
			if(arr[j]>arr[j+1]){
				swap(arr+j,arr+j+1);
				swapped=1;
			}

		if(swapped==0)
			break;

		swapped = 0;
		for(j=arrSize-1-i;j>i;j--)
			if(arr[j]<arr[j-1]){
				swap(arr+j,arr+j-1);
				swapped=1;
			}
		i++;
	}
	return;
}
//---------------------------------------------------------------------------------------------

//------------------------------ODD EVEN SORT--------------------------------------------------
//Odd Even Sort --- bubble sort done in odd and even slots separately instead of concurrently
void oddEvenSort(int *arr, int arrSize){
	int i=0, j=0, swapped = 1;
	while(swapped==1){
		swapped = 0;
		for(j=1;j<arrSize-1;j=j+2)
			if(arr[j]>arr[j+1]){
				swap(arr+j, arr+j+1);
				swapped = 1;
			}

		for(j=0;j<arrSize-1;j=j+2)
			if(arr[j]>arr[j+1]){
				swap(arr+j, arr+j+1);
				swapped = 1;
			}
	}

	return;
}
//---------------------------------------------------------------------------------------------

//-------------------------------COMB SORT-----------------------------------------------------
//CombSort 
void combSort(int *arr, int arrSize){
	int gap = arrSize, swapped = 0, i=0;
	while(gap>1||swapped==1){
		gap = (int) (gap/1.2473);
		if(gap<1)
			gap = 1;
		swapped = 0;

		for(i=0;i+gap<arrSize;i++)
			if(arr[i]>arr[gap+i]){
				swap(arr+i, arr+i+gap);
				swapped = 1;
			}
	}
	return;
}
//---------------------------------------------------------------------------------------------

//-------------------------------GNOME SORT----------------------------------------------------
//GnomeSort || StupidSort
void gnomeSort(int *arr, int arrSize){
	int p = 1;
	int last = 0; //
	while(p<arrSize){
		if(arr[p]>=arr[p-1]){
			if(last!=0){//
				p = last;//
				last = 0;//
			}//
			p++;}
		else{
			swap(arr+p, arr+p-1);
			if(p>1){
				if(last==0)//
					last = p;//
				p--;}
			else
				p++;
		}
	}
}
//---------------------------------------------------------------------------------------------

//------------------------------QUICK SORT-----------------------------------------------------
//Quick Sort ---
void Qsort(int v[], int left, int right){
	int i, last;

	if(left>=right)
		return;

	pswap(v,left, (left+right)/2);
	last = left;

	for(i=left+1; i<right; i++)
		if(v[i] < v[left])
			pswap(v, ++last, i);
	
	pswap(v, left, last);
	
	Qsort(v, left, last);
	Qsort(v, last+1, right);
}

//Quick Sort initiliazer
void quickSort(int *arr, int arrSize){
	Qsort(arr, 0, arrSize);
}
//----------------------------------------------------------------------------------------------

//----------------------------------MERGE SORT--------------------------------------------------
//Merge Sort Reduce function -- Merge
void merge(int *a, int *b, int size){
	int iter, *x, *p, *q;
	x = (int*) malloc(size*sizeof(int));
	for(iter=0,p=a,q=b; iter<size; iter++)
		x[iter] = p == b  ? *q++ : q == a + size ? *p++ : *p < *q ? *p++ : *q++;
	memcpy(a, x, size * sizeof (int));
	free(x);
}

//Merge Sort Map function --- Split
void mergeSort(int *arr, int arrSize){
	if(arrSize>1){
		mergeSort(arr, arrSize/2);
		mergeSort(arr+arrSize/2, arrSize-arrSize/2);
		merge(arr, arr+arrSize/2, arrSize);
	}
}
//----------------------------------------------------------------------------------------------

//----------------------------------SHELL SORT--------------------------------------------------
//Shell Sort ---- Insertion sort with a twist of Gaps
void shellSort(int *arr, int arrSize){
	int inc = arrSize/2, i, j;
	while(inc>0){
		for(i=inc;i<arrSize;i++){
			int key = arr[i];
			j = i;
			while(j>=inc && arr[j-inc]>key){
				arr[j] = arr[j-inc];
				j = j - inc;
			}
			arr[j] = key;
		}

		inc = (int) inc/2.2;
	}
}
//-----------------------------------------------------------------------------------------------
